dist:
	USE_CYTHON=1 python setup.py sdist

wheel:
	USE_CYTHON=1 python setup.py bdist_wheel

win:
	USE_CYTHON=1 python setup.py bdist_wininst

build: dist
	pip install --upgrade dist/*

test: 
	make clean
	make build
	( cd pyrbfbound/test ; python tester2.py ; python test_shapes.py )

clean:
	rm -rf dist build
	rm -rf PyRBFBound.egg-info
	rm -f MANIFEST
