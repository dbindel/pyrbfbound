v0.1.0, 2014-07-01 -- Initial release
v0.1.1, 2014-07-18 -- Added derivatives
v0.1.2, 2014-07-23 -- 
 - Bug fix in extend_system
 - Definiteness check in factoriazations
 - Added scale parameter for Gaussian 
 - Hardy multiquadrics (with scale parameter)
 - Optional regularization parameter
v0.1.3, 2014-07-24 --
 - Fix to allow out-of-order equality constraint points initially
