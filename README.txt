=============================================================
PyRBFbound: RBF interpolation with bound constraints
=============================================================

PyRBFbound supports RBF interpolation with upper and lower
bound constraints at points.  Centers are included only
as needed to accommodate interpolation constraints or upper
or lower bound constraints.  A typical use case might
look like:

    import math
    import numpy as np
    from pyrbfbound import PyRBF, tps
    
    # Allow at most 100 points in 2D using thin-plate splines
    solver = PyRBF(100,2,phi=tps)

    # Set up a function to interpolate
    def f(x):
        return math.cos(x[0]) * math.sin(x[1]) - x[0]**2

    # Add at least three points that are equality constrained
    for j in range(3):
        x = np.random.rand(2)
        solver.new_point(x, f(x))

    # Now add some points with inequality constraints
    for j in range(100):
        x = np.random.rand(2)
        solver.new_point(x, f(x)-0.01, f(x)+0.01)

    # Now evaluate at several more points
    for j in range(100):
        x = np.random.rand(2)
        fxhat = solver.eval(x)
        print("Approx error: %.2e" % (fxhat-f(x)))

Points can be incrementally added over time, or have bounds tightened
or turned into equality constraints.


Building the package
--------------------

The PyRBFBound package uses Cython to interface to a C solver
implementation.  Assuming Cython is installed, one should be able to
build this package for OS X and Linux without much fuss.  Things are
more complicated under Windows.  It should be possible to build the
Python module yourself using `setup.py` under Windows if you have
installed the MinGW compiler (this is installed by default if you use
Anaconda under Windows).  Optionally, one can also use the
pre-compiled binary package, which should work with most Python
distributions under Windows.  The pre-compiled package is produced by
running `make win` under an appropriately configured MinGW development
environment.
