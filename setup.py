try:
    from setuptools import setup
    from setuptools.extension import Extension
except ImportError:
    from distutils.core import setup
    from distutils.extension import Extension

import os
import numpy

USE_CYTHON = os.getenv("USE_CYTHON")
ext = '.pyx' if USE_CYTHON else '.c'

extensions = [
    Extension("pyrbfbound", 
              [ "pyrbfbound/pyrbfbound" + ext, 
                "pyrbfbound/support.c" ],
              include_dirs=[numpy.get_include()]),
]

if USE_CYTHON:
    from Cython.Build import cythonize
    extensions = cythonize(extensions)

setup(
    name = 'PyRBFBound',
    version = '0.1.2',
    author='David Bindel',
    author_email='bindel@cornell.edu',
    description = 'RBF interpolation with inequality constraints.',
    long_description=open('README.txt').read(),
    url='http://bitbucket.org/dbindel/pyrbfbound',
    license='LICENSE.txt',
    ext_modules = extensions,
)
