"""
.. module:: pyrbfbound
   :synopsis: Python RBF interpolation with bounds.
.. moduleauthor:: David Bindel <bindel@cornell.edu>
"""

# Copyright (c) 2014 David Bindel.  
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import math
import numpy as np
cimport numpy as np
import scipy.spatial as scp

# ============ External C routines from support.c =======================

cdef extern from "support.h":

    void eval_cubic_rbf(double* fr, double* r, int N)
    double eval_cubic(double* c, double* x, double* xi, int n, int N)
    void eval_dcubic(double* g, double* c, double* x, double* xi, int n, int N)
    void eval_Hcubic(double* g, double* c, double* x, double* xi, int n, int N)

    void eval_tps_rbf(double* fr, double* r, int N)
    double eval_tps(double* c, double* x, double* xi, int n, int N)
    void eval_dtps(double* g, double* c, double* x, double* xi, int n, int N)
    void eval_Htps(double* g, double* c, double* x, double* xi, int n, int N)

    void eval_gauss_rbf(double* fr, double* r, int N, double s2)
    double eval_gauss(double* c, double* x, double* xi, int n, int N, double s2)
    void eval_dgauss(double* g, double* c, double* x, double* xi, int n, int N,
                     double s2)
    void eval_Hgauss(double* H, double* c, double* x, double* xi, int n, int N,
                     double s2)

    void eval_hardy_rbf(double* fr, double* r, int N, double gamma2)
    double eval_hardy(double* c, double* x, double* xi, int n, int N, 
                      double gamma2)
    void eval_dhardy(double* g, double* c, double* x, double* xi, int n, int N,
                     double gamma2)
    void eval_Hhardy(double* H, double* c, double* x, double* xi, int n, int N,
                     double gamma2)

    int factor_PLUm(int ldA, int N1, int N, double* A, int* ipiv)
    int extend_PLUm(int ldA, int N, double* A, int* ipiv)
    void reconstruct_PLUm(int ldLU, int N, double* LU, int* ipiv,
                           int ldA, double* A)
    int interp_solve(int ldA, int N, double* A, int* ipiv,
                     int* p, int* pref,
                     double* cr, double* lr, double* ur)
    void apply_perm_PLUm(int ldA, int N, double* A, int* ipiv, 
                         int js, int jd, int* p, int* p_old,
                         double* scratch)
    void chol2lu_PLUm(int ldA, int N, double* A, int* ipiv)


# ============ Python-C wrappers for support.c    =======================

def factor_PLUm_func(int N1, int N,
                     np.ndarray[double, ndim=2, mode="c"] A not None, 
                     np.ndarray[int, ndim=1, mode="c"] ipiv not None):
    status = factor_PLUm(A.shape[0], N1, N,
                         <double*> np.PyArray_DATA(A),
                         <int*> np.PyArray_DATA(ipiv))
    assert status >= 0, "Definiteness failure in LU mixed factor (%d)" % (status)
                  
def extend_PLUm_func(int N,
                     np.ndarray[double, ndim=2, mode="c"] A not None, 
                     np.ndarray[int, ndim=1, mode="c"] ipiv not None):
    status = extend_PLUm(A.shape[0], N,
                         <double*> np.PyArray_DATA(A),
                         <int*> np.PyArray_DATA(ipiv))
    assert status >= 0, "Definiteness failure extending LU mixed factor"

def reconstruct_PLUm_func(int N,
                          np.ndarray[double, ndim=2, mode="c"] LU not None,
                          np.ndarray[int, ndim=1, mode="c"] ipiv not None):
     A = np.zeros([N,N])
     reconstruct_PLUm(LU.shape[0], N,
                      <double*> np.PyArray_DATA(LU),
                      <int*> np.PyArray_DATA(ipiv),
                      N,
                      <double*> np.PyArray_DATA(A))
     return A
    

def interp_solve_func(int N,
                      np.ndarray[double, ndim=2, mode="c"] A not None, 
                      np.ndarray[int, ndim=1, mode="c"] ipiv not None,
                      np.ndarray[int, ndim=1, mode="c"] p not None,
                      np.ndarray[int, ndim=1, mode="c"] pref not None,
                      np.ndarray[double, ndim=1, mode="c"] cr not None, 
                      np.ndarray[double, ndim=1, mode="c"] lr not None, 
                      np.ndarray[double, ndim=1, mode="c"] ur not None):
    return interp_solve(A.shape[0], N, 
                 <double*> np.PyArray_DATA(A), 
                 <int*> np.PyArray_DATA(ipiv), 
                 <int*> np.PyArray_DATA(p), 
                 <int*> np.PyArray_DATA(pref), 
                 <double*> np.PyArray_DATA(cr), 
                 <double*> np.PyArray_DATA(lr), 
                 <double*> np.PyArray_DATA(ur))

def apply_perm_PLUm_func(int N, int js, int jd,
                         np.ndarray[double, ndim=2, mode="c"] A not None, 
                         np.ndarray[int, ndim=1, mode="c"] ipiv not None,
                         np.ndarray[int, ndim=1, mode="c"] p not None,
                         np.ndarray[int, ndim=1, mode="c"] p_old not None):
    ldA = A.shape[0]
    scratch = np.zeros([N])
    apply_perm_PLUm(ldA, N,
                    <double*> np.PyArray_DATA(A),
                    <int*> np.PyArray_DATA(ipiv),
                    js, jd,
                    <int*> np.PyArray_DATA(p),
                    <int*> np.PyArray_DATA(p_old),
                    <double*> np.PyArray_DATA(scratch))

def chol2lu_PLUm_func(int N, 
                      np.ndarray[double, ndim=2, mode="c"] A not None, 
                      np.ndarray[int, ndim=1, mode="c"] ipiv not None):
    ldA = A.shape[0]
    chol2lu_PLUm(ldA, N,
                 <double*> np.PyArray_DATA(A),
                 <int*> np.PyArray_DATA(ipiv))

# ============ Basis functions and derivs =======================

class CubicRBF(object):
    "Represent a cubic radial basis function."

    def order(self):
        return 2

    def phi(self, x):
        "Compute phi(r) where phi is the cubic RBF."
        return x ** 3

    def eval(self, np.ndarray[double, ndim=1, mode="c"] c not None,
             np.ndarray[double, ndim=2, mode="c"] x not None,
             np.ndarray[double, ndim=1, mode="c"] xi not None):
        """Evaluate s(xi) where s is a cubic RBF interpolant.
        NB: This does not include a polynomial tail!
        
        Args:
            c: coefficients for centers
            x: list of centers
            xi: evaluation point
        """
        cdef size_t N = c.shape[0]
        cdef size_t n = x.shape[1]
        assert xi.shape[0] == n
        assert x.shape[0] == N
        return eval_cubic(<double*> np.PyArray_DATA(c),
                          <double*> np.PyArray_DATA(x),
                          <double*> np.PyArray_DATA(xi), n, N)
        
    def grad(self, np.ndarray[double, ndim=1, mode="c"] c not None,
             np.ndarray[double, ndim=2, mode="c"] x not None,
             np.ndarray[double, ndim=1, mode="c"] xi not None):
        """Evaluate grad s(xi) where s is a cubic RBF interpolant.
        NB: This does not include a polynomial tail!
        
        Args:
            c: coefficients for centers
            x: list of centers
            xi: evaluation point
        """
        cdef size_t N = c.shape[0]
        cdef size_t n = x.shape[1]
        assert xi.shape[0] == n
        assert x.shape[0] == N
        g = np.zeros_like(xi)
        eval_dcubic(<double*> np.PyArray_DATA(g),
                    <double*> np.PyArray_DATA(c),
                    <double*> np.PyArray_DATA(x),
                    <double*> np.PyArray_DATA(xi),
                    n, N)
        return g

    def hessian(self, np.ndarray[double, ndim=1, mode="c"] c not None,
                np.ndarray[double, ndim=2, mode="c"] x not None,
                np.ndarray[double, ndim=1, mode="c"] xi not None):
        """Evaluate Hessian s(xi) where s is a cubic RBF interpolant.
        NB: This does not include a polynomial tail!
        
        Args:
            c: coefficients for centers
            x: list of centers
            xi: evaluation point
        """
        cdef size_t N = c.shape[0]
        cdef size_t n = x.shape[1]
        assert xi.shape[0] == n
        assert x.shape[0] == N
        H = np.zeros((n, n))
        eval_Hcubic(<double*> np.PyArray_DATA(H),
                    <double*> np.PyArray_DATA(c),
                    <double*> np.PyArray_DATA(x),
                    <double*> np.PyArray_DATA(xi),
                    n, N)
        return H


class ThinPlateRBF(object):
    "Represent a thin-plate spline radial basis function."

    def order(self):
        return 2

    def phi(self, x):
        "Compute phi(r) where phi is the thin-plate RBF."
        x = np.asarray(x)
        result = np.zeros(x.shape)
        idx = np.nonzero(x)
        if np.any(idx):
            result[idx] = (x[idx] ** 2) * np.log(x[idx])
        return result

    def eval(self, np.ndarray[double, ndim=1, mode="c"] c not None,
             np.ndarray[double, ndim=2, mode="c"] x not None,
             np.ndarray[double, ndim=1, mode="c"] xi not None):
        """Evaluate s(xi) where s is a thin-plate RBF interpolant.
        NB: This does not include a polynomial tail!

        Args:
            c: coefficients for centers
            x: list of centers
            xi: evaluation point
        """
        cdef size_t N = c.shape[0]
        cdef size_t n = x.shape[1]
        assert xi.shape[0] == n
        assert x.shape[0] == N
        return eval_tps(<double*> np.PyArray_DATA(c),
                        <double*> np.PyArray_DATA(x),
                        <double*> np.PyArray_DATA(xi), n, N)
        
    def grad(self, np.ndarray[double, ndim=1, mode="c"] c not None,
             np.ndarray[double, ndim=2, mode="c"] x not None,
             np.ndarray[double, ndim=1, mode="c"] xi not None):
        """Evaluate grad s(xi) where s is a thin-plate RBF interpolant.
        NB: This does not include a polynomial tail!
        
        Args:
            c: coefficients for centers
            x: list of centers
            xi: evaluation point
        """
        cdef size_t N = c.shape[0]
        cdef size_t n = x.shape[1]
        assert xi.shape[0] == n
        assert x.shape[0] == N
        g = np.zeros_like(xi)
        eval_dtps(<double*> np.PyArray_DATA(g),
                  <double*> np.PyArray_DATA(c),
                  <double*> np.PyArray_DATA(x),
                  <double*> np.PyArray_DATA(xi),
                  n, N)
        return g

    def hessian(self, np.ndarray[double, ndim=1, mode="c"] c not None,
                np.ndarray[double, ndim=2, mode="c"] x not None,
                np.ndarray[double, ndim=1, mode="c"] xi not None):
        """Evaluate Hessian s(xi) where s is a thin-plate RBF interpolant.
        NB: This does not include a polynomial tail!
        
        Args:
            c: coefficients for centers
            x: list of centers
            xi: evaluation point
        """
        cdef size_t N = c.shape[0]
        cdef size_t n = x.shape[1]
        assert xi.shape[0] == n
        assert x.shape[0] == N
        H = np.zeros((n, n))
        eval_Htps(<double*> np.PyArray_DATA(H),
                  <double*> np.PyArray_DATA(c),
                  <double*> np.PyArray_DATA(x),
                  <double*> np.PyArray_DATA(xi),
                  n, N)
        return H


class GaussianRBF(object):
    "Represent a Gaussian radial basis function."

    def __init__(self, sigma = 1):
        """Initialize the Gaussian RBF.
        
        Args:
            sigma: Scale parameter for the RBF
        """
        self.sigma = sigma

    def order(self):
        return 0

    def phi(self, x):
        "Compute phi(r) where phi is the Gaussian RBF."
        cdef double s2 = self.sigma * self.sigma
        return np.exp((-0.5/s2) * (x ** 2))

    def eval(self, np.ndarray[double, ndim=1, mode="c"] c not None,
             np.ndarray[double, ndim=2, mode="c"] x not None,
             np.ndarray[double, ndim=1, mode="c"] xi not None):
        """Evaluate s(xi) where s is a Gaussian RBF interpolant.
        
        Args:
            c: coefficients for centers
            x: list of centers
            xi: evaluation point
        """
        cdef size_t N = c.shape[0]
        cdef size_t n = x.shape[1]
        cdef double s2 = self.sigma * self.sigma
        assert xi.shape[0] == n
        assert x.shape[0] == N
        return eval_gauss(<double*> np.PyArray_DATA(c),
                          <double*> np.PyArray_DATA(x),
                          <double*> np.PyArray_DATA(xi), n, N, s2)
        
    def grad(self, np.ndarray[double, ndim=1, mode="c"] c not None,
             np.ndarray[double, ndim=2, mode="c"] x not None,
             np.ndarray[double, ndim=1, mode="c"] xi not None):
        """Evaluate grad s(xi) where s is a Gaussian RBF interpolant.
        
        Args:
            c: coefficients for centers
            x: list of centers
            xi: evaluation point
        """
        cdef size_t N = c.shape[0]
        cdef size_t n = x.shape[1]
        cdef double s2 = self.sigma * self.sigma
        assert xi.shape[0] == n
        assert x.shape[0] == N
        g = np.zeros_like(xi)
        eval_dgauss(<double*> np.PyArray_DATA(g),
                    <double*> np.PyArray_DATA(c),
                    <double*> np.PyArray_DATA(x),
                    <double*> np.PyArray_DATA(xi),
                    n, N, s2)
        return g

    def hessian(self, np.ndarray[double, ndim=1, mode="c"] c not None,
                np.ndarray[double, ndim=2, mode="c"] x not None,
                np.ndarray[double, ndim=1, mode="c"] xi not None):
        """Evaluate Hessian s(xi) where s is a Gaussian RBF interpolant.
        
        Args:
            c: coefficients for centers
            x: list of centers
            xi: evaluation point
        """
        cdef size_t N = c.shape[0]
        cdef size_t n = x.shape[1]
        cdef double s2 = self.sigma * self.sigma
        assert xi.shape[0] == n
        assert x.shape[0] == N
        H = np.zeros((n, n))
        eval_Hgauss(<double*> np.PyArray_DATA(H),
                    <double*> np.PyArray_DATA(c),
                    <double*> np.PyArray_DATA(x),
                    <double*> np.PyArray_DATA(xi),
                    n, N, s2)
        return H

class HardyRBF(object):
    "Represent a Hardy multiquadric radial basis function."

    def __init__(self, gamma = 1):
        """Initialize the Hardy RBF.
        
        Args:
            gamma: Scale parameter for the RBF
        """
        self.gamma2 = gamma*gamma

    def order(self):
        return 1

    def phi(self, x):
        "Compute phi(r) where phi is the Gaussian RBF."
        return -np.sqrt(x ** 2 + self.gamma2)

    def eval(self, np.ndarray[double, ndim=1, mode="c"] c not None,
             np.ndarray[double, ndim=2, mode="c"] x not None,
             np.ndarray[double, ndim=1, mode="c"] xi not None):
        """Evaluate s(xi) where s is a Gaussian RBF interpolant.
        
        Args:
            c: coefficients for centers
            x: list of centers
            xi: evaluation point
        """
        cdef size_t N = c.shape[0]
        cdef size_t n = x.shape[1]
        assert xi.shape[0] == n
        assert x.shape[0] == N
        return eval_hardy(<double*> np.PyArray_DATA(c),
                          <double*> np.PyArray_DATA(x),
                          <double*> np.PyArray_DATA(xi), n, N, self.gamma2)
        
    def grad(self, np.ndarray[double, ndim=1, mode="c"] c not None,
             np.ndarray[double, ndim=2, mode="c"] x not None,
             np.ndarray[double, ndim=1, mode="c"] xi not None):
        """Evaluate grad s(xi) where s is a Gaussian RBF interpolant.
        
        Args:
            c: coefficients for centers
            x: list of centers
            xi: evaluation point
        """
        cdef size_t N = c.shape[0]
        cdef size_t n = x.shape[1]
        assert xi.shape[0] == n
        assert x.shape[0] == N
        g = np.zeros_like(xi)
        eval_dhardy(<double*> np.PyArray_DATA(g),
                    <double*> np.PyArray_DATA(c),
                    <double*> np.PyArray_DATA(x),
                    <double*> np.PyArray_DATA(xi),
                    n, N, self.gamma2)
        return g

    def hessian(self, np.ndarray[double, ndim=1, mode="c"] c not None,
                np.ndarray[double, ndim=2, mode="c"] x not None,
                np.ndarray[double, ndim=1, mode="c"] xi not None):
        """Evaluate Hessian s(xi) where s is a Gaussian RBF interpolant.
        
        Args:
            c: coefficients for centers
            x: list of centers
            xi: evaluation point
        """
        cdef size_t N = c.shape[0]
        cdef size_t n = x.shape[1]
        assert xi.shape[0] == n
        assert x.shape[0] == N
        H = np.zeros((n, n))
        eval_Hhardy(<double*> np.PyArray_DATA(H),
                    <double*> np.PyArray_DATA(c),
                    <double*> np.PyArray_DATA(x),
                    <double*> np.PyArray_DATA(xi),
                    n, N, self.gamma2)
        return H

# ============ Python RBF manager class   =======================

class PyRBF(object):
    """Represent an RBF interpolant.

    The RBF interpolant has a number of points at which equality or
    inequality constraints are satisfied.  The interpolant consists of
    a polynomial tail (only constant or linear terms are currently
    allowed), together with radial basis functions with centers at any
    points where the equality or inequality constraints are active.
    As points are added or the constraints are tightened, the
    interpolant can be (relatively) efficiently recomputed by updating
    a factorization of the interpolation system matrix.

    Attributes:
        rbf: Radial basis function (dictionary with phi, eval, grad, hess)
             Default is rbf_cubic; other options are rbf_tps, rbf_gauss.
        Nc: Number of points currently in the interpolant
        Nd: Number of points incorporated into the factorization
        dt: Degree of positive definiteness of the kernel
        NN: Current size of the factorization
        LU: The mixed LU/Cholesky factorization of the system matrix
        ipiv: Pivot information
        pref: Maps order in the factorization to the reference point order
        c: Coefficients in the interpolant
        l: Lower bounds at all points
        u: Upper bounds at all points
        x: Coordinates of all points
        stale: True if new information arrived since the last evaluation
    """    

    def __init__(self, Nmax, n, rbf=None, dt=-1, eta=0):
        """Initialize the RBF surface.
        
        Args:
            Nmax: Maximum number of points we can accomodate
            n:    Dimension of the ambient space
            rbf:  Radial basis function object
            dt:   Degree bound for polynomial tail (default: rbf.order())
            eta:  Regularization parameter
        """
        self.Nc = 0   # Points added
        self.Nd = 0   # Points done (incorporated in factor)

        if rbf is None:
            rbf = CubicRBF()
        if dt < 0:
            dt = rbf.order()
        self.dt = dt
        self.phi  = rbf.phi
        self.dphi = rbf.grad
        self.Hphi = rbf.hessian

        self.nb = 0
        self.Nmin = 0
        self.Nmax = Nmax
        if self.dt > 0:
            self.nb = self.nb + 1
            self.Nmin = 1
        if self.dt > 1:
            self.nb = self.nb + n
            self.Nmin = n+1
        if self.dt > 2:
            raise ValueError("Tail cannot be quadratic or higher")
        self.eta = 0

        NN = Nmax+self.nb
        self.NN = NN
        self.LU = np.zeros([NN,NN])
        self.ipiv = np.zeros([NN], dtype=np.intc)
        self.pref = np.intc([j for j in range(NN)])
        self.p = np.zeros([NN], dtype=np.intc)
        self.c = np.zeros([NN])
        self.u = np.zeros([NN])
        self.l = np.zeros([NN])
        self.x = np.zeros([Nmax,n])
        self.stale = True

    def new_point(self, xp, lp, up=None):
        """Add a new point to the surface and return an identifier.
        
        Args:
            xp:   Coordinates of the point
            lp:   Lower bound on the function value
            up:   Upper bound on the function.  If absent, assume up = lp.
        """
        if up is None:
            up = lp
        id = self.Nc
        self.Nc = self.Nc+1
        assert self.Nc <= self.Nmax, "Too many points."
        self.x[id,:] = xp
        self.l[self.nb+id] = lp
        self.u[self.nb+id] = up
        self.stale = True
        return id

    def set_point(self, id, lp, up=None):
        """Change constraints at a point.
        
        Args:
            id: Index of the point (as returned by new_point)
            lp: New lower bound on the function value
            up: New upper bound on the function.  If absent, assume up = lp.
        """
        if up is None:
             up = lp
        self.l[self.nb+id] = lp
        self.u[self.nb+id] = up
        self.stale = True

    def eval(self, x):
        """Evaluate the interpolant at a point or points.
        
        Args:
            x: Array of evaluation points.
        """
        if self.stale:
            self._solve()
            self.stale = False
        D = scp.distance.cdist(x, self.x[:self.Nc,:])
        c = self.c[self.nb:self.Nd+self.nb]
        fx = np.dot(self.phi(np.asarray(D)), c)
        if self.dt > 0:
            a0 = self.c[0]
            fx = fx + a0
        if self.dt > 1:
            a = self.c[1:self.nb]
            fx = fx + np.dot(x, a)
        return fx

    def grad(self, x):
        """Evaluate the gradient of the interpolant at a point."""
        if self.dt == 0:
            return self.dphi(self.c[:self.Nc], self.x[:self.Nc], x)
        elif self.dt == 1:
            return self.dphi(self.c[1:self.Nc+1], self.x[:self.Nc], x)
        elif self.dt == 2:
            g = self.c[1:self.nb]
            return g+self.dphi(self.c[self.nb:self.Nc+self.nb], 
                               self.x[:self.Nc], x)

    def hessian(self, x):
        """Evaluate the Hessian of the interpolant at a point."""
        if self.dt == 0:
            return self.Hphi(self.c[:self.Nc], self.x[:self.Nc], x)
        elif self.dt == 1:
            return self.Hphi(self.c[1:self.Nc+1], self.x[:self.Nc], x)
        elif self.dt == 2:
            return self.Hphi(self.c[self.nb:self.Nc+self.nb], 
                             self.x[:self.Nc], x)

    def check(self, quiet=True):
        """Check the interpolant behavior.

        Currently this prints the values of c, f, l, and u at each point.
        """
        fx = self.eval(self.x[:self.Nc,:])
        NN = self.nb+self.Nc
        fl = np.minimum(fx-self.l[self.nb:NN], 1e8)
        uf = np.minimum(self.u[self.nb:NN]-fx, 1e8)
        cp = np.maximum( self.c[self.nb:NN], 0)
        cm = np.maximum(-self.c[self.nb:NN], 0)
        maxerr = max(-min(fl),0)
        maxerr = max(-min(uf),maxerr)
        maxerr = max(abs(np.dot(cp,fl)), maxerr)
        maxerr = max(abs(np.dot(cm,uf)), maxerr)
        if not quiet:
            print("===")
            print("Constraint violation:       %.2e %.2e" % 
                  (max(-min(fl),0), max(-min(uf),0)))
            print("Complementarity violation:  %.2e %.2e" %
                  (abs(np.dot(cp, fl)), abs(np.dot(cm, uf))))
            nb = self.nb
            marks = self._mark_equality_constraints()
            print(" ")
            print("  i :  c     f     l     u")
            print("--- :  ----  ----  ----  -----")
            for j in range(self.Nc):
                c = " "
                if marks[j+nb]:
                    c = "*"
                print("% 3d : % .2f % .2f % .2f % .2f  %s" % 
                      (j, self.c[j+nb], fx[j], self.l[j+nb], self.u[j+nb], c))
            print(" ")
        return maxerr

    def _get_N1(self):
        """Get the size of the LU part of the factorization."""
        N1 = 0
        for j in range(self.Nc+self.nb):
            if self.ipiv[j] == 0:
                break
            N1 = N1+1
        return N1

    def _sift(self):
        """Move one point into the LU part of the factorization."""
        N1 = self._get_N1()
        for j in range(N1, self.Nc+self.nb):
            pj = self.pref[j]
            if self.l[pj] == self.u[pj] and j >= N1:
                if j != N1:
                    apply_perm_PLUm_func(self.Nd + self.nb, j, N1,
                                               self.LU, self.ipiv, 
                                               self.p, self.pref)
                    self.pref[:] = self.p
                chol2lu_PLUm_func(self.Nd + self.nb, self.LU, self.ipiv)
                return True
        return False

    def _sort(self):
        """Move any equality-constrained points into the LU part."""
        while self._sift():
            pass

    def _extend(self):
        """Extend the factorization to incorporate new points."""
        if self.Nd == 0:
            self.Nd = self.Nc
            Nf = self.Nd + self.nb
            N1 = self.nb
            for j in range(self.nb,Nf):
                if self.l[j] == self.u[j]:
                    N1 = N1+1
            assert N1 >= self.nb + self.Nmin, "Not enough values to interpolate"
            self._setup_system()
            factor_PLUm_func(N1, Nf, self.LU, self.ipiv)
        else:
            for j in range(self.Nd,self.Nc):
                self.pref[j + self.nb] = j + self.nb
                self._extend_system(j)
                self.LU0 = np.copy(self.LU)
                extend_PLUm_func(j + self.nb, self.LU, self.ipiv)
            self.Nd = self.Nc

    def _setup_system(self):
        """Form the interpolation system."""
        self.LU[:self.nb,self.nb] = 0
        NN = self.nb + self.Nc
        gap = self.u-self.l
        idx = np.argsort(gap[self.nb:NN])
        self.pref[self.nb:NN] = self.pref[self.nb+idx]
        xx = self.x[self.pref[self.nb:NN]-self.nb,:]
        if self.dt > 0:
            self.LU[self.nb:NN,0] = 1
            self.LU[0,self.nb:NN] = 1
        if self.dt > 1:
            self.LU[self.nb:NN,1:self.nb] = xx
            self.LU[1:self.nb,self.nb:NN] = xx.T
        D = scp.distance.cdist(xx, xx)
        self.LU[self.nb:NN,self.nb:NN] = self.phi(D)
        for j in range(self.nb,NN):
            self.LU[j,j] = self.LU[j,j] + self.eta

    def _extend_system(self, j):
        """Extend the interpolation system by one row/col."""
        NN = self.nb + j
        if self.dt > 0:
            self.LU[NN,0] = 1
            self.LU[0,NN] = 1
        if self.dt > 1:
            self.LU[NN,1:self.nb] = self.x[j,:]
            self.LU[1:self.nb,NN] = self.x[j,:].T
        D = scp.distance.cdist(self.x[[j],:], 
                               self.x[self.pref[self.nb:NN]-self.nb,:])
        self.LU[NN,self.nb:NN] = np.ravel(self.phi(D))
        self.LU[self.nb:NN,NN] = self.LU[NN,self.nb:NN].T
        self.LU[NN,NN] = self.phi(0) + self.eta

    def _solve(self):
        """Solve the linear system for interpolation."""
        self._extend()
        self._sort()
        Nf = self.Nd + self.nb
        status = interp_solve_func(Nf, self.LU, self.ipiv, self.p, self.pref,
                                   self.c, self.l, self.u)
        if status < 0:
            print("Warning: Possible nonconvergence.")
        self.pref[:] = self.p

    def _mark_equality_constraints(self):
        """Mark all equations subject to equality constraints."""
        marks = np.zeros([self.Nc+self.nb], dtype=np.bool_)
        for j in range(self.Nc+self.nb):
            if self.ipiv[j] > 0:
                pj = self.pref[j]
                marks[pj] = True
        return marks
