import numpy as np
import pyrbf

A = np.zeros([15,15])
x = np.array([[1.], [2.], [3.], [4.], [5.], [6.], [7.], [8.], [9.], [10.]])
u = np.array([0.0, 0.0, 0.61828, 1.10121, 1.1373,
              0.815545, 1.38815, 1.32385, 0.718329,
              0.800023, 0.90006, 0.497284])
l = np.array([0.0, 0.0, 0.16828, 0.651209, 0.6873,
              0.365545, 0.93815, 0.873846, 0.268329,
              0.350023, 0.45006, 0.0472837])
c = np.zeros([12])
pref = np.intc([0,1,2,3,4,5,6,7,8,9,10,11])
p    = np.zeros([12], dtype=np.intc)
ipiv = np.zeros([12], dtype=np.intc)

# Set up reference version of A
A0 = np.zeros([15,15])
pyrbf.interp_system_func(A0, x)

def init_solve(l, u, x, N1, N2):
    LU = np.zeros([N2,N2])
    pyrbf.interp_system_func(LU,x[:N2-x.shape[1]-1])
    ipiv = np.zeros([N2], dtype=np.intc)
    pyrbf.factor_PLUm_func(N1, N2, LU, ipiv)
    c = np.zeros([N2])
    pref = np.intc([j for j in range(N2)])
    p = np.zeros([N2], dtype=np.intc)
    pyrbf.interp_solve_func(N2, LU, ipiv, p, pref, c, l, u)
    return LU, ipiv, p, c

def update_solve(LU, ipiv, pref, c0, l, u, N2):
    LU = np.copy(LU)
    p = np.zeros([LU.shape[0]], dtype=np.intc)
    c = np.zeros([LU.shape[0]])
    c[:c0.shape[0]] = c0
    pyrbf.interp_solve_func(N2, LU, ipiv, p, pref, c, l, u)
    return LU, ipiv, p, c

def print_solution(c):
    print("===")
    N = c.shape[0]
    f = np.dot(A0[:N,:N],c[:N])
    for j in range(N):
        print("% .2f\t% .2f\t% .2f\t% .2f" % (c[j], f[j], l[j], u[j]))

def extend_system(LU0, ipiv0, pref0, x, N2):
    N3 = N2+1
    LU = np.zeros([N3,N3])
    ipiv = np.zeros([N3], dtype=np.intc)
    pref = np.zeros([N3], dtype=np.intc)
    LU[:N2,:N2] = LU0
    ipiv[:N2] = ipiv0
    pref[:N2] = pref0
    pref[N2] = N2
    pyrbf.extend_interp_system_func(N2-x.shape[1]-1, LU, x, pref)
    pyrbf.extend_PLUm_func(N2, LU, ipiv)
    return LU, ipiv, pref

# Solve a small problem from scratch
N1 = 4
N2 = 7
LU, ipiv, p, c = init_solve(l, u, x, N1, N2)
print_solution(c)

# Warm-started solution
l[2] = 0.20
LU, ipiv, p, c = update_solve(LU, ipiv, p, c, l, u, N2)
print_solution(c)

N3 = N2+1

# Extend system
for k in range(3):
    LU, ipiv, p = extend_system(LU, ipiv, p, x, N2)
    LU, ipiv, p, c = update_solve(LU, ipiv, p, c, l, u, N3)
    print_solution(c)
    N2 = N2+1
    N3 = N3+1
