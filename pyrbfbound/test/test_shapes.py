import numpy as np
import numpy.linalg as la
from pyrbfbound import CubicRBF, ThinPlateRBF, GaussianRBF, HardyRBF

def fd_grad(phi, x, h):
    g = np.zeros(x.shape)
    d = np.zeros(x.shape)
    for j in range(np.size(x)):
        d[j] = h
        g[j] = (phi(x+d)-phi(x-d))/(2*h)
        d[j] = 0
    return g

def fd_hess(phi, x, h):
    H = np.zeros([np.size(x), np.size(x)])
    d = np.zeros(x.shape)
    for j in range(np.size(x)):
        d[j] = h
        gp = fd_grad(phi, x+d, h)
        gm = fd_grad(phi, x-d, h)
        H[:,j] = (gp-gm)/(2*h)
        d[j] = 0
    return H

print "-- Test shape deriv consistency --"

x = np.random.rand(5,3)
c = np.random.rand(5)
xi = np.random.rand(3)
h = 1e-4

def tester(rbf):

    def testf(xi):
        return rbf.eval(c, x, xi)

    g_fd = fd_grad(testf, xi, h)
    H_fd = fd_hess(testf, xi, h)
    g = rbf.grad(c, x, xi)
    H = rbf.hessian(c, x, xi)

    print la.norm(g_fd-g)
    print la.norm(H_fd-H)

tester(CubicRBF())
tester(ThinPlateRBF())
tester(GaussianRBF(0.23))
tester(HardyRBF(0.23))
