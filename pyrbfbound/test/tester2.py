import numpy as np
import scipy.spatial as scp
from pyrbfbound import PyRBF, GaussianRBF

x = np.array([[1.], [2.], [3.], [4.], [5.], [6.], [7.], [8.], [9.], [10.]])
u = np.array([0.61828, 1.10121, 1.1373,
              0.815545, 1.38815, 1.32385, 0.718329,
              0.800023, 0.90006, 0.497284])
#l = np.array([0.16828, 0.651209, 0.6873,
#              0.365545, 0.93815, 0.873846, 0.268329,
#              0.350023, 0.45006, 0.0472837])
l = np.array([0.16828, 0.651209, 0.6873,
              0.365545, 0.03815, 0.873846, 0.268329,
              0.350023, 0.45006, 0.0472837])
u[:2] = l[:2]

quiet = True
solver = PyRBF(12,1, rbf=GaussianRBF(), dt=0)
for j in range(5):
    solver.new_point(x[j,:], l[j], u[j])
print(solver.check(quiet))

solver.set_point(0, 0.1, 0.1)
print(solver.check(quiet))

for j in range(5,10):
    solver.new_point(x[j,:], l[j], u[j])
print(solver.check(quiet))

solver.set_point(8, 1.0)
print(solver.check(quiet))

solver.set_point(9, 1.1, 1.1)
print(solver.check(quiet))
