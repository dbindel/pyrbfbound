import numpy as np
import numpy.linalg as la

def fd_grad(phi, x, h):
    g = np.zeros(x.shape)
    d = np.zeros(x.shape)
    for j in range(np.size(x)):
        d[j] = h
        g[j] = (phi(x+d)-phi(x-d))/(2*h)
        d[j] = 0
    return g

def fd_hess(phi, x, h):
    H = np.zeros([np.size(x), np.size(x)])
    d = np.zeros(x.shape)
    for j in range(np.size(x)):
        d[j] = h
        gp = fd_grad(phi, x+d, h)
        gm = fd_grad(phi, x-d, h)
        H[:,j] = (gp-gm)/(2*h)
        d[j] = 0
    return H


def cubic(x):
    r = la.norm(x)
    return r ** 3

def dcubic(x):
    r = la.norm(x)
    return 3*r*x

def Hcubic(x):
    r = la.norm(x)
    u = np.asmatrix(np.ravel(x)).T / r
    return (3*r)*(np.eye(np.size(x)) + u*u.T)


def tps(x):
    r = la.norm(x)
    return r ** 2 * np.log(r)

def dtps(x):
    r = la.norm(x)
    return (2*np.log(r)+1)*x

def Htps(x):
    r = la.norm(x)
    u = np.asmatrix(np.ravel(x)).T / r
    return (2*np.log(r)+1)*np.eye(np.size(x)) + 2*u*u.T


def gaussian(x):
    r = la.norm(x)
    return np.exp(-0.5 * r ** 2)

def dgaussian(x):
    return -gaussian(x) * x

def Hgaussian(x):
    xx = np.asmatrix(np.ravel(x)).T
    return -gaussian(x) * (np.eye(np.size(x)) - xx*xx.T)


x = np.random.rand(3)

print la.norm( dcubic(x)-fd_grad(cubic, x, 1e-4) )
print la.norm( Hcubic(x)-fd_hess(cubic, x, 1e-4) )

print la.norm( dtps(x)-fd_grad(tps, x, 1e-4) )
print la.norm( Htps(x)-fd_hess(tps, x, 1e-4) )

print la.norm( dgaussian(x)-fd_grad(gaussian, x, 1e-4) )
print la.norm( Hgaussian(x)-fd_hess(gaussian, x, 1e-4) )
