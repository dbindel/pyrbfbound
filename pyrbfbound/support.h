#ifndef SUPPORT_H
#define SUPPORT_H

void eval_cubic_rbf(double* fr, double* r, int N);
double eval_cubic(double* c, double* x, double* xi, int n, int N);
void eval_dcubic(double* g, double* c, double* x, double* xi, int n, int N);
void eval_Hcubic(double* g, double* c, double* x, double* xi, int n, int N);

void eval_tps_rbf(double* fr, double* r, int N);
double eval_tps(double* c, double* x, double* xi, int n, int N);
void eval_dtps(double* g, double* c, double* x, double* xi, int n, int N);
void eval_Htps(double* g, double* c, double* x, double* xi, int n, int N);

void eval_gauss_rbf(double* fr, double* r, int N, double s2);
double eval_gauss(double* c, double* x, double* xi, int n, int N, double s2);
void eval_dgauss(double* g, double* c, double* x, double* xi, int n, int N,
                 double s2);
void eval_Hgauss(double* H, double* c, double* x, double* xi, int n, int N,
                 double s2);

void eval_hardy_rbf(double* fr, double* r, int N, double gamma2);
double eval_hardy(double* c, double* x, double* xi, int n, int N, 
                  double gamma2);
void eval_dhardy(double* g, double* c, double* x, double* xi, int n, int N,
                 double gamma2);
void eval_Hhardy(double* H, double* c, double* x, double* xi, int n, int N,
                 double gamma2);

int factor_PLUm(int ldA, int N1, int N, double* A, int* ipiv);
int extend_PLUm(int ldA, int N, double* A, int* ipiv);
void reconstruct_PLUm(int ldLU, int N, double* LU, int* ipiv,
                       int ldA, double* A);
int interp_solve(int ldA, int N, double* A, int* ipiv,
                 int* p, int* pref,
                 double* cr, double* lr, double* ur);
void apply_perm_PLUm(int ldA, int N, double* A, int* ipiv, 
                     int js, int jd, int* p, int* p_old,
                     double* scratch);
void chol2lu_PLUm(int ldA, int N, double* A, int* ipiv);

#endif /* SUPPORT_H */
