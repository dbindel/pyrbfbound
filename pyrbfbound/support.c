/*
RBF-AS -- Active-set solver for RBF interpolation with bound constraints

Copyright (c) 2014 David Bindel.  

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

/*
 * ======================================== 
 * A note regarding LAPACK/BLAS
 * ======================================== 
 *
 * Ideally, I would like to use LAPACK/BLAS calls for the linear algebra
 * in these routines.  Where a routine is mostly a standard LAPACK/BLAS call,
 * there's a marker in the code.  Unfortunately, getting a build system that
 * works reliably with LAPACK/BLAS, Python, and Cython appears to still be
 * problematic at the time of this writing -- numpy.distutils (which knows
 * where LAPACK lives) doesn't play so well with Cython.  Maybe the right
 * approach is to use CMake, but I've already spent too much time on this.
 */

/*
 * ======================================== 
 * Common basis functions and derivs
 * ======================================== 
 */

/*
 * Evaluate a cubic RBF for N radii
 */
void eval_cubic_rbf(double* fr, double* r, int N)
{
    int i;
    for (i = 0; i < N; ++i) {
        double ri = r[i];
        fr[i] = ri*ri*ri;
    }
}

/*
 * For the cubic function phi(r) = r^3 and x in R^n, 
 * evaluate s(xi) = sum_{j=1}^N c[j] phi(|xi-x[j]|).
 */
double eval_cubic(double* c, double* x, double* xi, int n, int N)
{
    int i, j;
    double s = 0;
    for (i = 0; i < N; ++i) {
        double r = 0;
        for (j = 0; j < n; ++j) {
            double di = xi[j]-x[j];
            r += di*di;
        }
        r = sqrt(r);
        s += c[i] * r*r*r;
        x += n;
    }
    return s;
}

/*
 * For the cubic function phi(r) = r^3 and x in R^n, 
 * evaluate grad s(xi) where s(xi) = sum_{j=1}^N c[j] phi(|xi-x[j]|).
 *
 * Note that
 *   grad phi(d) = 3 r d
 * where d = |r|.
 */
void eval_dcubic(double* g, double* c, double* x, double* xi, int n, int N)
{
    int i, j;

    /* Clear storage for gradient */
    for (j = 0; j < n; ++j)
        g[j] = 0;

    /* Accumulate gradient */
    for (i = 0; i < N; ++i) {
        double s = 0;
        for (j = 0; j < n; ++j) {
            double dj = xi[j]-x[j];
            s += dj*dj;
        }
        s = 3*c[i]*sqrt(s);
        for (j = 0; j < n; ++j) {
            double dj = xi[j]-x[j];
            g[j] += s*dj;
        }
        x += n;
    }
}

/*
 * For the cubic function phi(r) = r^3 and x in R^n, 
 * evaluate Hessian s(xi) where s(xi) = sum_{j=1}^N c[j] phi(|xi-x[j]|)
 *
 * Note that
 *   Hess phi(d) = 3r(I-uu') = 3r I - 3/r dd'
 * where r = |d| and u = d/r.
 */
void eval_Hcubic(double* H, double* c, double* x, double* xi, int n, int N)
{
    int i, j, k;

    /* Clear storage for Hessian */
    for (j = 0; j < n*n; ++j)
        H[j] = 0;

    /* Accumulate Hessian */
    for (i = 0; i < N; ++i) {

        /* Compute distance to x point */
        double r = 0, c1, c2;
        for (j = 0; j < n; ++j) {
            double dj = xi[j]-x[j];
            r += dj*dj;
        }
        r = sqrt(r);

        /* Add Hessian contribution */
        if (r > 0) {
            c1 = 3*c[i]*r;
            c2 = 3*c[i]/r;
            for (j = 0; j < n; ++j) {
                double dj = xi[j]-x[j];
                H[j*n+j] += c1;
                for (k = 0; k < n; ++k) {
                    double dk = xi[k]-x[k];
                    H[k*n+j] += c2*dj*dk;
                }
            }
        }
        x += n;

    }
}


/*
 * Evaluate a thin-plate spline RBF for N radii: phi = r^2 log r
 */
void eval_tps_rbf(double* fr, double* r, int N)
{
    int i;
    for (i = 0; i < N; ++i) {
        double ri = r[i];
        fr[i] = ri*ri * log(ri);
    }
}


/*
 * For the thin-plate splite function phi and x in R^n, 
 * evaluate s(xi) = sum_{j=1}^N c[j] phi(|xi-x[j]|).
 */
double eval_tps(double* c, double* x, double* xi, int n, int N)
{
    int i, j;
    double s = 0;
    for (i = 0; i < N; ++i) {
        double r2 = 0;
        for (j = 0; j < n; ++j) {
            double di = xi[j]-x[j];
            r2 += di*di;
        }
        s += c[i] * r2 * log(r2)/2;
        x += n;
    }
    return s;
}

/*
 * For the thin plate spline function phi and x in R^n
 * evaluate grad s(xi) where s(xi) = sum_{j=1}^N c[j] phi(|xi-x[j]|).
 *
 * Note that
 *   grad phi(d) = (log(r^2)+1) d
 * where d = |r|.
 */
void eval_dtps(double* g, double* c, double* x, double* xi, int n, int N)
{
    int i, j;

    /* Clear storage for gradient */
    for (j = 0; j < n; ++j)
        g[j] = 0;

    /* Accumulate gradient */
    for (i = 0; i < N; ++i) {
        double s = 0;
        for (j = 0; j < n; ++j) {
            double dj = xi[j]-x[j];
            s += dj*dj;
        }
        if (s != 0)
            s = c[i]*(log(s)+1);
        for (j = 0; j < n; ++j) {
            double dj = xi[j]-x[j];
            g[j] += s*dj;
        }
        x += n;
    }
}


/*
 * For the thin plate spline  function phi and x in R^n, 
 * evaluate Hessian s(xi) where s(xi) = sum_{j=1}^N c[j] phi(|xi-x[j]|)
 *
 * Note that
 *   Hess phi(d) = (log r^2 + 1) I + 2 d d' / r^2
 * where r = |d|.
 */
void eval_Htps(double* H, double* c, double* x, double* xi, int n, int N)
{
    int i, j, k;

    /* Clear storage for Hessian */
    for (j = 0; j < n*n; ++j)
        H[j] = 0;

    /* Accumulate Hessian */
    for (i = 0; i < N; ++i) {

        /* Compute distance to x point */
        double r2 = 0, c1, c2;
        for (j = 0; j < n; ++j) {
            double dj = xi[j]-x[j];
            r2 += dj*dj;
        }

        /* No Hessian at r = 0, but we'll fib. */
        if (r2 == 0) {
            c1 = -100*c[i];
            c2 = 0;
        } else {
            c1 = c[i]*(log(r2)+1);
            c2 = 2*c[i]/r2;
        }

        /* Add Hessian contribution */
        for (j = 0; j < n; ++j) {
            double dj = xi[j]-x[j];
            H[j*n+j] += c1;
            for (k = 0; k < n; ++k) {
                double dk = xi[k]-x[k];
                H[k*n+j] += c2*dj*dk;
            }
        }
        x += n;

    }
}


/*
 * Evaluate a Gaussian RBF for N radii: phi = exp(-r^2/sigma^2)
 */
void eval_gauss_rbf(double* fr, double* r, int N, double s2)
{
    int i;
    for (i = 0; i < N; ++i) {
        double ri = r[i];
        fr[i] = exp(-0.5/s2 * ri*ri);
    }
}


/*
 * For the Gaussian function phi and x in R^n, 
 * evaluate s(xi) = sum_{j=1}^N c[j] phi(|xi-x[j]|).
 */
double eval_gauss(double* c, double* x, double* xi, int n, int N, double s2)
{
    int i, j;
    double s = 0;
    for (i = 0; i < N; ++i) {
        double r2 = 0;
        for (j = 0; j < n; ++j) {
            double di = xi[j]-x[j];
            r2 += di*di;
        }
        s += c[i] * exp(-0.5/s2 * r2);
        x += n;
    }
    return s;
}


/*
 * For the Gaussian function phi and x in R^n
 * evaluate grad s(xi) where s(xi) = sum_{j=1}^N c[j] phi(|xi-x[j]|).
 *
 * Note that
 *   grad phi(d) = -phi(d) d
 * where d = |r|.
 */
void eval_dgauss(double* g, double* c, double* x, double* xi, int n, int N,
                 double s2)
{
    int i, j;

    /* Clear storage for gradient */
    for (j = 0; j < n; ++j)
        g[j] = 0;

    /* Accumulate gradient */
    for (i = 0; i < N; ++i) {
        double s = 0;
        for (j = 0; j < n; ++j) {
            double dj = xi[j]-x[j];
            s += dj*dj;
        }
        s = -c[i] * exp(-0.5/s2*s);
        for (j = 0; j < n; ++j) {
            double dj = xi[j]-x[j];
            g[j] += s*dj;
        }
        x += n;
    }
    
    /* Scale gradient */
    for (j = 0; j < n; ++j)
        g[j] /= s2;
}


/*
 * For the Gaussian function phi and x in R^n, 
 * evaluate Hessian s(xi) where s(xi) = sum_{j=1}^N c[j] phi(|xi-x[j]|)
 *
 * Note that
 *   Hess phi(d) = -phi(r) (I-d d')
 * where r = |d|.
 */
void eval_Hgauss(double* H, double* c, double* x, double* xi, int n, int N,
                 double s2)
{
    int i, j, k;

    /* Clear storage for Hessian */
    for (j = 0; j < n*n; ++j)
        H[j] = 0;

    /* Accumulate Hessian */
    for (i = 0; i < N; ++i) {

        /* Compute distance to x point */
        double r2 = 0, s;
        for (j = 0; j < n; ++j) {
            double dj = xi[j]-x[j];
            r2 += dj*dj;
        }

        /* Add Hessian contribution */
        s = -c[i] * exp(-0.5/s2*r2) / s2;
        for (j = 0; j < n; ++j) {
            double dj = xi[j]-x[j];
            H[j*n+j] += s;
            for (k = 0; k < n; ++k) {
                double dk = xi[k]-x[k];
                H[k*n+j] -= s*dj*dk/s2;
            }
        }
        x += n;

    }
}


/*
 * Evaluate a multiquadric RBF for N radii: phi = -sqrt(r^2 + gamma^2)
 */
void eval_hardy_rbf(double* fr, double* r, int N, double gamma2)
{
    int i;
    for (i = 0; i < N; ++i) {
        double ri = r[i];
        fr[i] = -sqrt(ri*ri + gamma2);
    }
}


/*
 * For the multiquadric function phi and x in R^n, 
 * evaluate s(xi) = sum_{j=1}^N c[j] phi(|xi-x[j]|).
 */
double eval_hardy(double* c, double* x, double* xi, int n, int N, double gamma2)
{
    int i, j;
    double s = 0;
    for (i = 0; i < N; ++i) {
        double r2 = 0;
        for (j = 0; j < n; ++j) {
            double di = xi[j]-x[j];
            r2 += di*di;
        }
        s += -c[i] * sqrt(r2 + gamma2);
        x += n;
    }
    return s;
}


/*
 * For the multiquadric function phi and x in R^n
 * evaluate grad s(xi) where s(xi) = sum_{j=1}^N c[j] phi(|xi-x[j]|).
 *
 * Note that
 *   grad phi(d) = d/phi(d)
 * where d = |r|.
 */
void eval_dhardy(double* g, double* c, double* x, double* xi, int n, int N,
                 double gamma2)
{
    int i, j;

    /* Clear storage for gradient */
    for (j = 0; j < n; ++j)
        g[j] = 0;

    /* Accumulate gradient */
    for (i = 0; i < N; ++i) {
        double s = 0;
        for (j = 0; j < n; ++j) {
            double dj = xi[j]-x[j];
            s += dj*dj;
        }
        s = -c[i]/sqrt(s + gamma2);
        for (j = 0; j < n; ++j) {
            double dj = xi[j]-x[j];
            g[j] += s*dj;
        }
        x += n;
    }
}


/*
 * For the multiquadric function phi and x in R^n, 
 * evaluate Hessian s(xi) where s(xi) = sum_{j=1}^N c[j] phi(|xi-x[j]|)
 *
 * Note that
 *   Hess phi(d) = (I-d d'/phi^2)/phi
 * where r = |d|.
 */
void eval_Hhardy(double* H, double* c, double* x, double* xi, int n, int N,
                 double gamma2)
{
    int i, j, k;

    /* Clear storage for Hessian */
    for (j = 0; j < n*n; ++j)
        H[j] = 0;

    /* Accumulate Hessian */
    for (i = 0; i < N; ++i) {

        /* Compute distance to x point */
        double r2 = 0, iphi, iphi2, s, s2;
        for (j = 0; j < n; ++j) {
            double dj = xi[j]-x[j];
            r2 += dj*dj;
        }
        iphi2 = 1.0/(r2 + gamma2);
        iphi = -sqrt(iphi2);

        /* Add Hessian contribution */
        s = c[i]*iphi;
        s2 = s*iphi2;
        for (j = 0; j < n; ++j) {
            double dj = xi[j]-x[j];
            H[j*n+j] += s;
            for (k = 0; k < n; ++k) {
                double dk = xi[k]-x[k];
                H[k*n+j] -= s2*dj*dk;
            }
        }
        x += n;

    }
}


/*
 * ======================================== 
 * Cubic interpolant system
 * ======================================== 
 *
 * This is not actually needed in the Python code, since there is a Python
 * implementation that works pretty well.  Keeping it just in case.
 */


/*
 * Euclidean distance in n-dimensional space
 */
double dist(int n,       /* Dimension of space    */
            double* xa,  /* Coordinates of points */
            double* xb)
{
    int k;
    double d = 0;
    for (k = 0; k < n; ++k) {
        double xak = xa[k];
        double xbk = xb[k];
        d += (xak-xbk)*(xak-xbk);
    }
    return sqrt(d);
}


/*
 * Form the bordered system matrix
 *   [ 0, P'   ;
 *     P, Phi ]
 * where P_{ij} is the jth basis polynomial at point i
 * for a linear space, and Phi_{ij} is phi(|x_i-x_j|),
 * where in this case phi(r) = r^3
 */
void interp_system(int ldA,     /* Leading dimension of A */
                   int N,       /* Number of points */
                   int n,       /* Dimension of space */
                   double* A,   /* Output storage for interp system */
                   double* xx)  /* List of coordinates of center points */
{
    int i, j;
    int nb = n+1;
    int NN = N+nb;

    /* Clear (1,1) block */
    for (i = 0; i < nb; ++i)
        for (j = 0; j < nb; ++j)
            A[i+j*ldA] = 0;

    for (j = 0; j < N; ++j) {

        /* Fill in border */
        A[(j+nb)*ldA] = 1;
        for (i = 0; i < n; ++i)
            A[i+1+(j+nb)*ldA] = xx[i+j*n];

        /* Fill interior */
        for (i = 0; i < j; ++i) {
            double d = dist(n, xx+i*n, xx+j*n);
            d = d*d*d;
            A[(i+nb)+(j+nb)*ldA] = d;
        }

        /* Fill diagonal */
        A[(j+nb)+(j+nb)*ldA] = 0;
    }

    /* Fill symmetric part */
    for (i = 0; i < NN; ++i)
        for (j = 0; j < i; ++j)
            A[i+j*ldA] = A[j+i*ldA];
}


/*
 * Add one point to the interpolation system.
 */
void extend_interp_system(int ldA,    /* Leading dimension of A */
                          int N,      /* Number of points before extension */
                          int n,      /* Dimension of the space */
                          double* A,  /* Storage for system matrix */
                          double* xx, /* Center point coordinates */
                          int* p)     /* Maps A index to coordinate index */
{
    int i;
    int nb = n+1;
    int NN = N+nb;

    /* Fill in border */
    A[NN*ldA] = 1;
    A[NN    ] = 1;
    for (i = 0; i < n; ++i) {
        A[(i+1)+NN*ldA] = xx[i+N*n];
        A[NN+(i+1)*ldA] = xx[i+N*n];
    }
    
    /* Fill interior */
    for (i = 0; i < N; ++i) {
        int pi = p[i+nb]-nb;
        double d = dist(n, xx+pi*n, xx+N*n);
        d = d*d*d;
        A[(i+nb)+NN*ldA] = d;
        A[NN+(i+nb)*ldA] = d;
    }
    A[NN+NN*ldA] = 0;
}


/*
 * ======================================== 
 * Data movement (cyclic permutations)
 * ======================================== 
 */

/*
 * Apply a cycle-right permutation on a length N double vector
 */
void cycle_right_dv(int N,     /* Vector length */
                    double* x, /* Start of vector */
                    int incx)  /* Vector stride */
{
    int i;
    double t = x[(N-1)*incx];
    for (i = N-1; i > 0; --i)
        x[i*incx] = x[(i-1)*incx];
    x[0] = t;
}


/*
 * Apply a cycle-left permutation on a length N double vector
 */
void cycle_left_dv(int N,     /* Vector length */
                   double* x, /* Start of vector */
                   int incx)  /* Vector stride */
{
    int i;
    double t = x[0];
    for (i = 0; i < N-1; ++i)
        x[i*incx] = x[(i+1)*incx];
    x[(N-1)*incx] = t;
}


/*
 * Apply a cycle-right permutation on a length N integer vector
 */
void cycle_right_iv(int N, int* x)
{
    int i;
    int t = x[N-1];
    for (i = N-1; i > 0; --i)
        x[i] = x[i-1];
    x[0] = t;
}


/*
 * Apply a cycle-left permutation on a length N integer vector
 */
void cycle_left_iv(int N, int* x)
{
    int i;
    int t = x[0];
    for (i = 0; i < N-1; ++i)
        x[i] = x[i+1];
    x[N-1] = t;
}


/*
 * Apply a cycle-right permutation to columns of an M-by-N matrix A
 */
void cycle_right_col(int ldA, int M, int N, double* A)
{
    int i;
    for (i = 0; i < M; ++i)
        cycle_right_dv(N, A+i, ldA);
}


/*
 * Apply a cycle-left permutation to columns of an M-by-N matrix A
 */
void cycle_left_col(int ldA, int M, int N, double* A)
{
    int i;
    for (i = 0; i < M; ++i)
        cycle_left_dv(N, A+i, ldA);
}


/*
 * Apply a cycle-right permutation to rows of an M-by-N matrix A
 */
void cycle_right_row(int ldA, int M, int N, double* A)
{
    int j;
    for (j = 0; j < N; ++j)
        cycle_right_dv(M, A+j*ldA, 1);
}


/*
 * Apply a cycle-left permutation to rows of an M-by-N matrix A
 */
void cycle_left_row(int ldA, int M, int N, double* A)
{
    int j;
    for (j = 0; j < N; ++j)
        cycle_left_dv(M, A+j*ldA, 1);
}


/*
 * ======================================== 
 * Cholesky factorization and updates
 * ======================================== 
 */

/*
 * Clear the lower triangle of an N-by-N matrix A
 */
void clear_lower(int ldA, int N, double* A)
{
    int i, j;
    for (j = 0; j < N; ++j)
        for (i = j+1; i < N; ++i)
            A[i+j*ldA] = 0;
}


/*
 * Compute A = R^T R where A is N-by-N
 * Return 0 if OK, -k < 0 if a negative pivot encountered at step k.
 * NB: This should be a DPOTRF call
 */
int factor_RTR(int ldA, int N, double* A)
{
    int i, j, k;
    clear_lower(ldA, N, A);
    for (j = 0; j < N && j < N; ++j) {
        double ajj = A[j+j*ldA];
        if (ajj < 0)
            return -(j+1);
        ajj = sqrt(ajj);
        A[j+j*ldA] = ajj;
        for (i = j+1; i < N; ++i)
            A[j+i*ldA] /= ajj;
        for (k = j+1; k < N; ++k)
            for (i = j+1; i <= k; ++i)
                A[i+k*ldA] -= A[j+i*ldA] * A[j+k*ldA];
    }
    return 0;
}


/*
 * Compute and apply a Givens rotation.  Assumes x, y not both zero.
 * NB: This should be a DLARTG or DROTG call.
 */
void compute_apply_givens(double* c, double* s, double* x, double* y)
{
    double cc = *x;
    double ss = -*y;
    double l = sqrt(cc*cc + ss*ss);
    cc /= l;
    ss /= l;
    *c = cc;
    *s = ss;
    *x = l;
    *y = 0;
}


/*
 * Apply a Givens rotation
 */
void apply_givens(double c, double s, double* x, double* y)
{
    double xx = *x;
    double yy = *y;
    *x = c*xx-s*yy;
    *y = s*xx+c*yy;
}


/*
 * Normalize R to have positive diagonals
 */
void normalize_R(int ldA, int N, double* A)
{
    int i, j;
    for (i = 0; i < N; ++i)
        if (A[i+i*ldA] < 0)
            for (j = i; j < N; ++j)
                A[i+j*ldA] = -A[i+j*ldA];
}


/*
 * Cycle right columns j1 <= j < j2 and return to triangular form
 */
void cycle_right_R(int ldR, int N, double* R, int j1, int j2)
{
    int i, j;
    cycle_right_col(ldR, N, j2-j1, R+j1*ldR);
    for (i = j2-1; i > j1; --i) {
        double c, s;
        compute_apply_givens(&c, &s, R+(i-1)+j1*ldR, R+i+j1*ldR);
        for (j = i; j < N; ++j)
            apply_givens(c, s, R+(i-1)+j*ldR, R+i+j*ldR);
    }
    normalize_R(ldR, N, R);
}


/*
 * Cycle left columns j1 <= j < j2 and return to triangular form
 */
void cycle_left_R(int ldR, int N, double* R, int j1, int j2)
{
    int i, j;
    cycle_left_col(ldR, N, j2-j1, R+j1*ldR);
    for (i = j1; i < j2-1; ++i) {
        double c, s;
        compute_apply_givens(&c, &s, R+i+i*ldR, R+(i+1)+i*ldR);
        for (j = i+1; j < N; ++j)
            apply_givens(c, s, R+i+j*ldR, R+(i+1)+j*ldR);
    }
    normalize_R(ldR, N, R);
}


/*
 * ======================================== 
 * Multiply and solve with Cholesky
 * ======================================== 
 */

/*
 * Compute x := R*x where R is N-by-N and x is length N
 *
 * NB: This should be a DTRMV call
 */
void times_R(int ldR, int N, double* R, double* x, int incx)
{
    int i, j;
    for (j = 0; j < N; ++j) {
        for (i = 0; i < j; ++i)
            x[i*incx] += R[i+j*ldR]*x[j*incx];
        x[j*incx] *= R[j+j*ldR];
    }
}


/*
 * Compute x := R^T*x where R is N-by-N and x is length N
 *
 * NB: This should be a DTRMV call
 */
void times_RT(int ldR, int N, double* R, double* x, int incx)
{
    int i, j;
    for (j = N-1; j >= 0; --j) {
        x[j*incx] *= R[j+j*ldR];
        for (i = 0; i < j; ++i)
            x[j*incx] += R[i+j*ldR]*x[i*incx];
    }
}


/*
 * Compute x := A*x where A = R^T R
 */
void times_RTR(int ldR, int N, double* R, double* x, int incx)
{
    times_R(ldR, N, R, x, incx);
    times_RT(ldR, N, R, x, incx);
}


/*
 * Compute x := R\x
 *
 * NB: This should be a DTRSV call
 */
void solve_R(int ldR, int N, double* R, double* x, int incx)
{
    int i, j;
    for (j = N-1; j >= 0; --j) {
        x[j*incx] /= R[j+j*ldR];
        for (i = 0; i < j; ++i)
            x[i*incx] -= R[i+j*ldR]*x[j*incx];
    }
}


/*
 * Compute x := R'\x
 *
 * NB: This should be a DTRSV call
 */
void solve_RT(int ldR, int N, double* R, double* x, int incx)
{
    int i, j;
    for (j = 0; j < N; ++j) {
        for (i = 0; i < j; ++i)
            x[j*incx] -= R[i+j*ldR]*x[i*incx];
        x[j*incx] /= R[j+j*ldR];
    }
}


/*
 * Compute x := A\x where A = R'R
 */
void solve_RTR(int ldR, int N, double* R, double* x, int incx)
{
    solve_RT(ldR, N, R, x, incx);
    solve_R(ldR, N, R, x, incx);
}


/*
 * ======================================== 
 * LU factorization
 * ======================================== 
 */

/*
 * Swap double vectors
 */
void swap_dv(int N, double* x, int incx, double* y, int incy)
{
    int i;
    for (i = 0; i < N; ++i) {
        double t = x[i*incx];
        x[i*incx] = y[i*incy];
        y[i*incy] = t;
    }
}


/*
 * Compute PA = LU for an M-by-N rectangular matrix
 *
 * NB: This should be a DGETRF call; IPIV is formatted accordingly
 * (i.e. using one-based indexing).
 */
int factor_PLU(int ldA, int M, int N, double* A, int* ipiv)
{
    int i, j;
    for (j = 0; j < N && j < M; ++j) {
        double* aj = A+j*ldA;

        /* Pivot in leading block (assume nonsingularity) */
        int k = j;
        for (i = j+1; i < M; ++i)
            if (fabs(aj[i]) > fabs(aj[k]))
                k = i;
        ipiv[j] = k+1;
        if (k != j)
            swap_dv(N, A+j, ldA, A+k, ldA);

        /* Exit with error status on zero pivot */
        if (aj[j] == 0)
            return -(j+1);

        /* Compute multipliers + update Schur complement */
        for (i = j+1; i < M; ++i)
            aj[i] /= aj[j];
        for (k = j+1; k < N; ++k)
            for (i = j+1; i < M; ++i)
                A[i+k*ldA] -= aj[i] * A[j+k*ldA];
    }
    return 0;
}


/*
 * Apply row interchanges recorded in ipiv to a vector v
 */
void apply_ipiv(int N, int* ipiv, double* v)
{
    int i;
    for (i = 0; i < N; ++i) {
        int k = ipiv[i]-1;
        if (k != i) {
            double tmp = v[i];
            v[i] = v[k];
            v[k] = tmp;
        }
    }
}


/*
 * ======================================== 
 * Solve via LU factorization
 * ======================================== 
 */

/*
 * Solve x := L\(Px) where L is unit lower triangular.
 *
 * NB: This should be DTRSV/DTRSM call.
 */
void solve_PL(int ldA, int N, double* A, int* ipiv, double* x)
{
    int i, j;
    apply_ipiv(N, ipiv, x);
    for (j = 0; j < N; ++j) {
        double* aj = A+j*ldA;
        for (i = j+1; i < N; ++i)
            x[i] -= aj[i]*x[j];
    }
}


/*
 * Solve x := A\x where PA = LU.
 *
 * NB: This should be DGESV call.
 */
void solve_PLU(int ldA, int N, double* A, int* ipiv, double* x)
{
    solve_PL(ldA, N, A, ipiv, x);
    solve_R(ldA, N, A, x, 1);
}


/*
 * ======================================== 
 * Mixed LU/Cholesky factorization
 * ======================================== 
 */

/*
 * Compute mixed LU/Cholesky factorization.
 * On a singularity or failure of positive definiteness,
 * return -k where k is the failure index.  Else return 0.
 */
int factor_PLUm(int ldA, int N1, int N, double* A, int* ipiv)
{
    int i, j, k;

    memset(ipiv, 0, N*sizeof(int));
    k = factor_PLU(ldA, N1, N, A, ipiv);
    if (k < 0)
        return k;

    /* Compute L21 (NB: Should be DTRSM call) */
    for (i = N1; i < N; ++i)
        solve_RT(ldA, N1, A, A+i, ldA);

    /* Compute Schur complement (NB: Should be DGEMM) */
    for (i = N1; i < N; ++i)
        for (j = N1; j < N; ++j)
            for (k = 0; k < N1; ++k)
                A[i+j*ldA] -= A[i+k*ldA] * A[k+j*ldA];

    /* Factor Schur complement */
    k = factor_RTR(ldA, N-N1, A+N1+N1*ldA);
    if (k < 0)
        return -N1-k;
    return 0;
}


/*
 * Find N1 from ipiv
 */
int find_PLUm_N1(int N, int* ipiv)
{
    int i;
    for (i = 0; i < N; ++i)
        if (ipiv[i] == 0)
            return i;
    return N;
}


/*
 * Extend mixed factorization by one (N is the old size).
 * Return negative error code if Cholesky fails, 0 otherwise.
 */
int extend_PLUm(int ldA, int N, double* A, int* ipiv)
{
    int i;
    double aNN;
    int N1 = find_PLUm_N1(N, ipiv);
    solve_PL(ldA, N1, A, ipiv, A+N*ldA);
    solve_RT(ldA, N, A, A+N, ldA);

    aNN = A[N+N*ldA];
    for (i = 0; i < N1; ++i)
        aNN -= A[N+i*ldA]*A[i+N*ldA];
    for (i = N1; i < N; ++i) {
        double aiN = A[N+i*ldA];
        A[i+N*ldA] = aiN;
        A[N+i*ldA] = 0;
        aNN -= aiN*aiN;
    }
    if (aNN < 0)
        return -(N+1);
    A[N+N*ldA] = sqrt(aNN);
    ipiv[N] = 0;
    return 0;
}


/* 
 * Convert a Cholesky part into an LU part of the factorization
 */
void chol2lu_PLUm(int ldA, int N, double* A, int* ipiv)
{
    int j;
    int N1 = find_PLUm_N1(N, ipiv);
    double t = A[N1+N1*ldA];
    for (j = N1; j < N; ++j)
        A[N1+j*ldA] *= t;
    t = A[N1+N1*ldA];
    for (j = N1+1; j < N; ++j)
        A[j+N1*ldA] = A[N1+j*ldA]/t;
    ipiv[N1] = N1+1;
}


/* 
 * Reconstruct the original matrix from PLUm
 */
void reconstruct_PLUm(int ldLU, int N, double* LU, int* ipiv,
                      int ldA, double* A)
{
    int i, j, k;
    int N1 = find_PLUm_N1(N, ipiv);

    /* Clear A */
    for (j = 0; j < N; ++j)
        for (i = 0; i < N; ++i)
            A[i+j*ldA] = 0;

    /* Form L(:,1) U(1,:) outer product */
    for (k = 0; k < N1; ++k) {
        for (j = k; j < N; ++j)
            A[k+j*ldA] += LU[k+j*ldLU];
        for (j = k; j < N; ++j)
            for (i = k+1; i < N; ++i)
                A[i+j*ldA] += LU[i+k*ldLU] * LU[k+j*ldLU];
    }

    /* Form R^T R part */
    for (k = N1; k < N; ++k) {
        for (j = k; j < N; ++j)
            for (i = k; i < N; ++i)
                A[i+j*ldA] += LU[k+i*ldLU] * LU[k+j*ldLU];
    }

    /* Apply ipiv */
    for (k = N1-1; k >= 0; --k) {
        int ik = ipiv[k]-1;
        if (ik != k)
            swap_dv(N, A+k, ldA, A+ik, ldA);
    }
}


/*
 * ======================================== 
 * Reordering helpers
 * ======================================== 
 *
 * In general, we will want to have three orderings: a reference ordering
 * associated with the order in which points come in; an old ordering
 * associated with whatever order things were in before a solve step;
 * and a new ordering associated with the permutation applied during
 * the solve.
 *
 * A pmap is a vector p that maps an index i in a current ordering
 * to an index p[i] in the reference ordering.
 */

void pmap_init(int N, int* p)
{
    int i;
    for (i = 0; i < N; ++i)
        p[i] = i;
}


void pmap_map_to_current(int N, int* p, int lo, int hi,
                         double* x, int incx,
                         double* y, int incy)
{
    int j;
    for (j = lo; j < hi; ++j)
        x[j*incx] = y[p[j]*incy];
}


void pmap_map_to_ref(int N, int* p, int lo, int hi,
                     double* x, int incx,
                     double* y, int incy)
{
    int j;
    for (j = lo; j < hi; ++j)
        x[p[j]*incx] = y[j*incy];
}


void pmap_permute_to_current(int N, int* p, int lo, int hi,
                             double* x, int incx, double* scratch)
{
    int j;
    pmap_map_to_current(N, p, lo, hi, scratch, 1, x, incx);
    for (j = lo; j < hi; ++j)
        x[j*incx] = scratch[j];
}


void pmap_compose(int N, int* p, int* p_old)
{
    int i;
    if (p_old)
        for (i = 0; i < N; ++i)
            p[i] = p_old[p[i]];
}


void pmap_shift(int N, int* p, int shift)
{
    int i;
    for (i = 0; i < N; ++i)
        p[i] += shift;
}

/*
 * ======================================== 
 * Reordering in mixed LU/Cholesky factorization
 * ======================================== 
 */

/*
 * Deferred application of permutations to L21 and U12
 *
 * Inputs:
 *   p  -- maps new indices to old indices
 *   scratch -- workspace of at least N doubles
 */
void apply_perm_deferred_PLUm(int ldA, int N, double* A, int* ipiv,
                              int* p, double* scratch)
{
    int i, j;
    int N1 = find_PLUm_N1(N, ipiv);
    for (i = 0; i < N1; ++i)
        pmap_permute_to_current(N, p, N1, N, A+i, ldA, scratch);
    for (j = 0; j < N1; ++j)
        pmap_permute_to_current(N, p, N1, N, A+j*ldA, 1, scratch);
}


/*
 * Permute indices in mixed factorization.
 *
 * Inputs:
 *   js -- initial (source) index of column to move
 *   jd -- destination index of column to move
 *   p  -- maps new indices to reference indices
 *   p_old -- maps old indices to reference indices (assume 0:N-1 if not given)
 *   scratch -- workspace of at least N doubles
 */
void apply_perm_PLUm(int ldA, int N, double* A, int* ipiv, 
                     int js, int jd, int* p, int* p_old,
                     double* scratch)
{
    int N1 = find_PLUm_N1(N, ipiv);
    pmap_init(N, p);

    /* Permute R and p */
    if (js < jd) {
        cycle_left_R(ldA, N-N1, A+N1+N1*ldA, js-N1, jd-N1+1);
        cycle_left_iv(jd-js+1, p+js);
    } else if (js > jd) {
        cycle_right_R(ldA, N-N1, A+N1+N1*ldA, jd-N1, js-N1+1);
        cycle_right_iv(js-jd+1, p+jd);
    } else 
        return;

    /* Apply deferred permutation to L21 and U12 */
    apply_perm_deferred_PLUm(ldA, N, A, ipiv, p, scratch);

    /* Map p := p_old[p] */
    pmap_compose(N, p, p_old);
}


/*
 * ======================================== 
 * Solution of complementarity problem
 * ======================================== 
 */

/*
 * Compute one component of the gradient of
 *   L(c) = c'*A*c/2 - min(c,0)'*l-max(c,0)'*u
 * Let g = A*c; then the gradient components are
 *   dL/dci = gi - l for c < 0
 *   dL/dci = gi - u for c > 0
 * For ci = 0 (a nonsmooth point) we return 0.
 *
 * Inputs:
 *   sc -- sign of one component of c
 *   g  -- one component of A*c
 *   l  -- one component of the l vector
 *   u  -- one component of the u vector
 */
double grad_nonsmooth(double sc, double g, double l, double u)
{
    if (sc > 0)
        return g-l;
    else if (sc < 0)
        return g-u;
    else
        return 0;
}


/*
 * Find the most strongly violated bound constraint for the current
 * solution.
 *
 * Inputs:
 *   N -- size of the problem
 *   g -- vector of function values (original indexing)
 *   l -- vector of lower bounds (original indexing)
 *   u -- vector of upper bounds (original indexing)
 *   p -- mapping from current indices to original indices
 * 
 * Returns:
 *   m -- index of the worst violation in the new indexing scheme.
 *        That is, min(g[p[m]]-l[p[m]], u[p[m]]-g[p[m]]) is as negative
 *        as possible.  Return zero if no violations < -tol.
 */
int worst_bound_violation(int N, double* g, double* l, double* u,
                          int* p, double tol)
{
    int j;
    int m = -1;
    double lmin = -tol;
    for (j = 0; j < N; ++j) {
        int pj = p[j];
        double gl = g[pj]-l[pj];
        double ug = u[pj]-g[pj];
        double ll = (gl < ug) ? gl : ug;
        if (ll < lmin) {
            m = j;
            lmin = ll;
        }
    }
    return m;
}


/*
 * Find the first binding constraint that would prevent us from
 * taking a full Newton step.
 *
 * Inputs:
 *   dp -- Newton direction (current indexing)
 *   c  -- Current solution (original indexing)
 *   p  -- mapping from current to original indexing
 *   Nc -- Number of active centers (where Newton is nonzero)
 * 
 * Returns:
 *   m -- first index s.t. c[p[m]]-alpha*dp[m] changes sign
 *        as alpha moves from zero to one.  We break ties
 *        arbitrarily.
 */
int find_step_size(double* dp, double* c, int* p, int Nc)
{
    int j;
    double alpha = 1;
    int m = -1;
    for (j = 0; j < Nc; ++j) {
        int pj = p[j];
        if (dp[j] != 0) {
            double aj = c[pj]/dp[j];
            if (aj > 0 && aj < alpha) {
                m = j;
                alpha = aj;
            }
        }
    }
    return m;
}


/*
 * Count the number of nonzero elements in c.
 */
int countnz(double* c, int N)
{
    int i;
    int nnz = 0;
    for (i = 0; i < N; ++i)
        if (c[i])
            ++nnz;
    return nnz;
}


/*
 * Get the signs of the elements of c.
 */
void get_signs(int N, double* sc, double* c)
{
    int i;
    memset(sc, 0, N * sizeof(double));
    for (i = 0; i < N; ++i) {
        if (c[i] > 0)
            sc[i] = 1;
        else if (c[i] < 0)
            sc[i] = -1;
    }
}


/*
 * Check whether a range contains the zero vector
 */
int contains_zero_vec(int N, double* l, double* u)
{
    int i;
    for (i = 0; i < N; ++i)
        if (l[i] > 0 || u[i] < 0)
            return 0;
    return 1;
}


/*
 * Minimize c^T A c - min(c,0)^T l - max(c,0)^T u by
 * an active set method.
 * 
 * Inputs:
 *   ldR -- leading dimension of R
 *   N   -- size of R
 *   R   -- Cholesky factor for permuted A
 *   p   -- Permutation mapping current -> original index
 *   c   -- initial guess
 *   l   -- lower bounds
 *   u   -- upper bounds
 * 
 * Outputs:
 *   R   -- Cholesky factor under new permutation
 *   p   -- Updated permutation mapping
 *   c   -- Solution vector
 * 
 * Returns:
 *   number of iterations to converge on success
 *   -maxiter on failure
 */
int complementarity_as(int ldR, int N, double* R, int* p, 
                       double* c, double* l, double* u)
{
    int i, j, m;
    int Nc = countnz(c, N);
    int maxiter = 20*N;
    double tol = 1e-12;

    /* Set up scratch space */
    double* scratch = malloc(6 * N * sizeof(double));
    double* g = scratch + 2*N;
    double* dp = scratch + 3*N;
    double* wr = scratch + 4*N;
    double* sc = scratch + 5*N;

    /* Check for early exit with trivial solution */
    if (contains_zero_vec(N, l, u)) {
        memset(c, 0, N * sizeof(double));
        free(scratch);
        return 0;
    }

    /* Set up sign */
    get_signs(N, sc, c);

    for (i = 0; i < maxiter; ++i) {

        double normwr = 0.0;
        memcpy(g, c, N * sizeof(double));
        memset(wr, 0, N * sizeof(double));

        /* Compute g using permuted Cholesky factor */
        pmap_map_to_current(N, p, 0, N, scratch, 1, g, 1);
        times_RTR(ldR, N, R, scratch, 1);
        pmap_map_to_ref(N, p, 0, N, g, 1, scratch, 1);

        /* Compute wr and norm(wr[sc .!= 0] */
        for (j = 0; j < Nc; ++j) {
            int pj = p[j];
            wr[pj] = grad_nonsmooth(sc[pj], g[pj], l[pj], u[pj]);
            if (normwr < fabs(wr[pj]))
                normwr = fabs(wr[pj]);
        }

        /* Constrained convergence check */
        if (normwr < tol) {
            m = worst_bound_violation(N, g, l, u, p, tol);
            if (m >= 0) {
                int pm = p[m];
                sc[pm] = (l[pm] > g[pm]) ? 1 : -1;
                wr[pm] = grad_nonsmooth(sc[pm], g[pm], l[pm], u[pm]);
                cycle_right_R(ldR, N, R, Nc, m+1);
                cycle_right_iv(m+1-Nc, p+Nc);
                ++Nc;
            } else
                break;
        }

        /* Compute Newton direction */
        pmap_map_to_current(N, p, 0, N, dp, 1, wr, 1);
        solve_RTR(ldR, Nc, R, dp, 1);
        m = find_step_size(dp, c, p, Nc);

        if (m < 0) {

            /* Take full Newton step */
            for (j = 0; j < Nc; ++j)
                c[p[j]] -= dp[j];

        } else {

            /* Add new constraint */
            int pm = p[m];
            double alpha = c[pm]/dp[m];
            for (j = 0; j < Nc; ++j)
                c[p[j]] -= alpha*dp[j];
            c[pm] = 0;
            sc[pm] = 0;
            --Nc;
            cycle_left_R(ldR, N, R, m, Nc+1);
            cycle_left_iv(Nc-m+1, p+m);

        }
    }

    free(scratch);
    return (i == maxiter ? -maxiter : i+1);
}


/*
 * ======================================== 
 * Solve the interpolation problem
 * ======================================== 
 */

int interp_solve(int ldA, int N, double* A, int* ipiv,
                 int* p, int* pref,
                 double* cr, double* lr, double* ur)
{
    int i, j, info = 0;
    int N1 = find_PLUm_N1(N, ipiv);

    /* Work space: scratch, c, l, u */
    double* scratch = malloc(4*N*sizeof(double));
    double* c = scratch +   N;
    double* l = scratch + 2*N;
    double* u = scratch + 3*N;

    /* Map solution estimate + bounds into current index scheme */
    pmap_init(N, p);
    pmap_map_to_current(N, pref, 0, N, c, 1, cr, 1);
    pmap_map_to_current(N, pref, 0, N, l, 1, lr, 1);
    pmap_map_to_current(N, pref, 0, N, u, 1, ur, 1);

    /* Save w := L11\(P11*fE) in first part of c */
    memcpy(c, l, N1*sizeof(double));
/*DSB: Don't think this is needed (see cr)?
    memset(c+N1, 0, (N-N1)*sizeof(double));
*/
    solve_PL(ldA, N1, A, ipiv, c);

    /* Save f0_B := L21*w */
    memset(scratch, 0, N*sizeof(double));
    for (j = 0; j < N1; ++j)
        for (i = N1; i < N; ++i)
            scratch[i] += A[i+j*ldA]*c[j];

    /* Form l' and u' */
    for (i = N1; i < N; ++i) {
        l[i] -= scratch[i];
        u[i] -= scratch[i];
    }

    /* Solve the complementarity problem */
    pmap_shift(N-N1, p+N1, -N1);
    info = complementarity_as(ldA, N-N1, A+N1+N1*ldA, 
                              p+N1, c+N1, l+N1, u+N1);
    pmap_shift(N-N1, p+N1, N1);

    /* Form w-U12*cB */
    for (j = N1; j < N; ++j)
        for (i = 0; i < N1; ++i)
            c[i] -= A[i+j*ldA] * c[j];
    
    /* Finish back substitution and write back results */
    solve_R(ldA, N1, A, c, 1);
    pmap_map_to_ref(N, pref, 0, N, cr, 1, c, 1);

    /* Bring the factorization back to a consistent state */
    apply_perm_deferred_PLUm(ldA, N, A, ipiv, p, scratch);
    pmap_compose(N, p, pref);

    /* Clean up and return */
    free(scratch);
    return info;
}
